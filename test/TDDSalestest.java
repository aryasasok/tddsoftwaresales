import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class TDDSalestest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	public void testBuy10Package()
	{
		TDDsoftwareslaes s = new TDDsoftwareslaes();
		double finalprice= s.calculatePrice(12);
		//when comparing decimals in Junit
		//you must add a 
		assertEquals(950.4, finalprice,0);
		
		
	}
	@Test
	public void testBuyOn20Package()
	{
		TDDsoftwareslaes s = new TDDsoftwareslaes();
		double finalprice= s.calculatePrice(30);
		//when comparing decimals in Junit
		//you must add a 
		assertEquals(2079, finalprice,0);
		
		
	}
	//R4 Buy 50-99 packages get 40 % 
	public void testBuyOn50Package()
	{
		TDDsoftwareslaes s = new TDDsoftwareslaes();
		double finalprice= s.calculatePrice(60);
		 
		assertEquals(3564, finalprice,0);
		
		
	}
	
	//R4 Buy 50-99 packages get 40 % 
		public void testBuyOn100Package()
		{
			TDDsoftwareslaes s = new TDDsoftwareslaes();
			double finalprice= s.calculatePrice(120);
			 
			assertEquals(5940, finalprice,0);
			
			
		}

}
